package al.marketlogic.news.utils.rx.exceptions

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NoResultsException: NoSuchElementException()