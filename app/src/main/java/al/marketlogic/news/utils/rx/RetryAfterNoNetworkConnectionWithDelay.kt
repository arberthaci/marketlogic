package al.marketlogic.news.utils.rx

import al.marketlogic.news.utils.rx.exceptions.NoNetworkConnectionException
import io.reactivex.Observable
import io.reactivex.functions.Function
import java.util.concurrent.TimeUnit

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class RetryAfterNoNetworkConnectionWithDelay (private val maxRetries: Int, var delay: Long, private val delayAmount: Long = 100)
                                    : Function<Observable<out Throwable>, Observable<*>> {

    private var retryCount = 0

    override fun apply(t: Observable<out Throwable>): Observable<*> {
        return t.flatMap {
            if (++retryCount < maxRetries && it is NoNetworkConnectionException) {
                delay += delayAmount
                Observable.timer(delay, TimeUnit.MILLISECONDS)
            } else {
                Observable.error(it)
            }
        }
    }
}