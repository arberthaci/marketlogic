package al.marketlogic.news.utils.extensions

import al.marketlogic.news.utils.AppConstants
import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

internal fun DateTime.toString(type: AppConstants.DateStringMode): String? {
    val months = arrayListOf("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

    return when (type) {
        AppConstants.DateStringMode.DATE_STRING_MODE -> { "${months[this.monthOfYear - 1]} ${this.dayOfMonth.toDayFormat()} ${this.year}" }
        AppConstants.DateStringMode.DATE_STRING_WITHOUT_YEAR_MODE -> {
            var yearString = ""
            val currentYear = DateTime()
            if (currentYear.year != this.year)
                yearString = " ${this.year}"

            "${months[this.monthOfYear - 1]} ${this.dayOfMonth.toDayFormat()}$yearString"
        }
        AppConstants.DateStringMode.DATE_NUMERIC_MODE -> { "${this.dayOfMonth}/" + "${this.monthOfYear}".padStart(2, '0') + "/${this.year}" }
        AppConstants.DateStringMode.DATE_JSON_MODE -> { "${this.year}-" + "${this.monthOfYear}".padStart(2, '0') + "-" + "${this.dayOfMonth}".padStart(2, '0') }
        AppConstants.DateStringMode.TIME_MODE -> { "${this.hourOfDay}".padStart(2, '0') + ":" + "${this.minuteOfHour}".padStart(2, '0') }
        AppConstants.DateStringMode.ONLY_MONTH_FULL_MODE -> { months[this.monthOfYear - 1] }
        AppConstants.DateStringMode.ONLY_MONTH_SHORT_MODE -> { months[this.monthOfYear - 1].take(3).toUpperCase() }
    }
}