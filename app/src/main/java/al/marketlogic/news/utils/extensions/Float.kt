package al.marketlogic.news.utils.extensions

import android.content.Context

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

internal fun Float.convertDpToPixel(context: Context): Int {
    val resources = context.resources
    val metrics = resources.displayMetrics
    return (this * (metrics.densityDpi / 160f)).toInt()
}