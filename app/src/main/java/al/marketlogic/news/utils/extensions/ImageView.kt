package al.marketlogic.news.utils.extensions

import al.marketlogic.news.R
import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

internal fun ImageView.loadImage(url: String?, resize: Boolean = false) {
    if (!resize)
        Picasso.with(this.context)
                .load(url.validatePhotoUrl())
                .placeholder(R.drawable.placeholder_loading)
                .error(R.drawable.placeholder_error)
                .into(this)
    else
        Picasso.with(this.context)
                .load(url.validatePhotoUrl())
                .placeholder(R.drawable.placeholder_loading)
                .error(R.drawable.placeholder_error)
                .resize(200, 200)
                .centerInside()
                .into(this)
}

internal fun ImageView.loadImage(uri: Uri) {
    Picasso.with(this.context)
            .load(uri)
            .placeholder(R.drawable.placeholder_loading)
            .error(R.drawable.placeholder_error)
            .into(this)
}

internal fun ImageView.loadImage(resourceId: Int) {
    Picasso.with(this.context)
            .load(resourceId)
            .placeholder(R.drawable.placeholder_loading)
            .error(R.drawable.placeholder_error)
            .into(this)
}