package al.marketlogic.news.utils.rx

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.data.network.model.FeedResponse
import al.marketlogic.news.ui.news.view.NewsMVPView
import al.marketlogic.news.utils.CommonUtils
import al.marketlogic.news.utils.customviews.ErrorViewState
import al.marketlogic.news.utils.rx.exceptions.NoNetworkConnectionException
import al.marketlogic.news.utils.rx.exceptions.NoResultsException
import android.content.Context
import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

object RxUtils {

    fun isNetworkConnected(context: Context): Observable<Boolean> =
            Observable.just(CommonUtils.isNetworkConnected(context))

    fun checkApiResponseData(feedResponse: FeedResponse): List<Article> {
        val totalResults = feedResponse.totalResults ?: 0
        val articlesSize = feedResponse.articles?.size ?: 0
        if (totalResults != 0 && articlesSize != 0)
            return feedResponse.articles ?: arrayListOf()
        else
            throw Exceptions.propagate(NoResultsException())
    }

    /**
     * ShoppingMVPView manageExceptions
     **/
    fun manageExceptions(throwable: Throwable, view: NewsMVPView) {
        when (throwable) {
            is NoNetworkConnectionException -> { view.showErrorView(ErrorViewState.NO_CONNECTION) }
            is NoResultsException -> { view.showErrorView(ErrorViewState.NO_RESULTS) }
            else -> { view.showErrorView(ErrorViewState.NO_RESPONSE) }
        }
    }
}