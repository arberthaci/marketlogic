package al.marketlogic.news.utils.rvadapterlisteners

import al.marketlogic.news.data.network.model.Article

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface OnNewsActionListener {

    fun onItemSelected(article: Article)
}