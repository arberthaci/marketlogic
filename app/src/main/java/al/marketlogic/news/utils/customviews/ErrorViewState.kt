package al.marketlogic.news.utils.customviews

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

enum class ErrorViewState {
    NO_ERROR,
    NO_CONNECTION,
    NO_RESULTS,
    NO_RESPONSE
}