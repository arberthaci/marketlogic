package al.marketlogic.news.utils.extensions

import al.marketlogic.news.BuildConfig

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

internal fun String?.validatePhotoUrl(): String {
    return if (!this.isNullOrBlank()) this
    else BuildConfig.BASE_URL
}