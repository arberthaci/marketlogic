package al.marketlogic.news.utils

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

object AppConstants {

    internal const val SHARED_PREFS_NAME = "marketlogic_shared_prefs"

    enum class AlerterMode constructor(val type: Int) {
        ALERT_INFO(1),
        ALERT_WARNING(2),
        ALERT_SUCCESS(3),
        ALERT_FAILED(4)
    }

    enum class FragmentAnimationMode constructor(val type: Int) {
        ANIMATION_NONE(0),
        ANIMATION_FADE(1),
        ANIMATION_SLIDE(2)
    }

    enum class DateStringMode constructor(val type: Int) {
        DATE_STRING_MODE(0),
        DATE_STRING_WITHOUT_YEAR_MODE(1),
        DATE_NUMERIC_MODE(2),
        DATE_JSON_MODE(3),
        TIME_MODE(4),
        ONLY_MONTH_FULL_MODE(5),
        ONLY_MONTH_SHORT_MODE(6)
    }

    enum class LoadingMode constructor(val type: Int) {
        FULL_LOADING(1),
        PAGINATION_LOADING(2),
        KEYWORD_SEARCH(3),
        SWIPE_TO_REFRESH(4)
    }
}