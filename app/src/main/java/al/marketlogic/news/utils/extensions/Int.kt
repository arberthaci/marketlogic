package al.marketlogic.news.utils.extensions

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

internal fun Int.toDayFormat(): String {
    return when (this) {
        1, 21, 31 -> "${this}st"
        2, 22 -> "${this}nd"
        3, 23 -> "${this}rd"
        else -> "${this}th"
    }
}