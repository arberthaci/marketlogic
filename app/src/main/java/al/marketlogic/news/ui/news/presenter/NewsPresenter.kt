package al.marketlogic.news.ui.news.presenter

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.base.presenter.BasePresenter
import al.marketlogic.news.ui.news.interactor.NewsMVPInteractor
import al.marketlogic.news.ui.news.view.NewsMVPView
import al.marketlogic.news.utils.AppConstants
import al.marketlogic.news.utils.rx.RetryAfterNoNetworkConnectionWithDelay
import al.marketlogic.news.utils.rx.RxUtils
import al.marketlogic.news.utils.rx.SchedulerProvider
import al.marketlogic.news.utils.rx.exceptions.NoNetworkConnectionException
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.Exceptions
import java.util.HashMap
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NewsPresenter <V : NewsMVPView, I : NewsMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), NewsMVPPresenter<V, I> {

    override fun onViewPrepared() {
        retrieveNewsFeed(page = 1, loadingMode = AppConstants.LoadingMode.FULL_LOADING)
    }

    override fun onRefreshFeed(keyword: String) {
        retrieveNewsFeed(page = 1, keyword = keyword, loadingMode = AppConstants.LoadingMode.SWIPE_TO_REFRESH)
    }

    override fun afterTextChanged(input: String) {
        if (input.isBlank()) {
            getView()?.idleSearchIcon()
            getView()?.hideClearIcon()
        }
        else {
            getView()?.activateSearchIcon()
            getView()?.showClearIcon()

            retrieveNewsFeed(page = 1, keyword = input, loadingMode = AppConstants.LoadingMode.KEYWORD_SEARCH)
        }
    }

    override fun onClearSearchClicked() {
        getView()?.clearSearchInput()
    }

    override fun onRetrieveNewsFeed(page: Int, keyword: String) {
        retrieveNewsFeed(page = page, keyword = keyword, loadingMode = AppConstants.LoadingMode.PAGINATION_LOADING)
    }

    override fun onArticleSelected(article: Article) {
        getView()?.openArticleFragment(article = article)
    }

    private fun retrieveNewsFeed(page: Int, keyword: String? = null, loadingMode: AppConstants.LoadingMode) {
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = page.toString()
        if (!keyword.isNullOrBlank()) {
            filters["q"] = keyword
        }

        getView()?.let {
            manageLoadingIndicator(view = it, loadingMode = loadingMode)

            compositeDisposable.add(it.isNetworkConnected()
                    .switchMap { isConnected ->
                        if (isConnected) {
                            interactor!!.callRetrieveTopHeadlines(filterParameters = filters)
                                    .map { feedResponse -> RxUtils.checkApiResponseData(feedResponse) }
                        }
                        else throw Exceptions.propagate(NoNetworkConnectionException())
                    }
                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe (
                            { articlesResponse ->
                                manageOnSuccess(view = it, loadingMode = loadingMode, articlesResponse = articlesResponse, page = page)
                            },
                            { err ->
                                manageOnError(view = it, loadingMode = loadingMode, throwable = err)
                            }
                    )
            )
        }
    }

    private fun manageLoadingIndicator(view: NewsMVPView, loadingMode: AppConstants.LoadingMode) {
        when (loadingMode) {
            AppConstants.LoadingMode.FULL_LOADING -> view.prepareLoading()
            AppConstants.LoadingMode.PAGINATION_LOADING -> view.showLoadMoreIndicator()
            AppConstants.LoadingMode.KEYWORD_SEARCH -> view.showLoadMoreIndicator()
            AppConstants.LoadingMode.SWIPE_TO_REFRESH -> { /* do nothing */ }
        }
    }

    private fun manageOnSuccess(view: NewsMVPView, loadingMode: AppConstants.LoadingMode, articlesResponse: List<Article>, page: Int) {
        if (page == 1) {
            interactor?.deleteArticlesFromRealm()
            interactor?.insertArticlesInRealm(articles = articlesResponse)

            view.displayNewsFirstPage(articles = articlesResponse)
        }
        else {
            interactor?.insertArticlesInRealm(articles = articlesResponse)

            view.displayNewsNextPage(articles = articlesResponse)
        }

        when (loadingMode) {
            AppConstants.LoadingMode.FULL_LOADING -> view.dismissLoadingIndicator()
            AppConstants.LoadingMode.PAGINATION_LOADING -> view.hideLoadMoreIndicator()
            AppConstants.LoadingMode.KEYWORD_SEARCH -> view.hideLoadMoreIndicator()
            AppConstants.LoadingMode.SWIPE_TO_REFRESH -> { view.dismissSwipeToRefreshIndicator() }
        }
    }

    private fun manageOnError(view: NewsMVPView, loadingMode: AppConstants.LoadingMode, throwable: Throwable) {
        when (loadingMode) {
            AppConstants.LoadingMode.FULL_LOADING -> {
                val mArticleResponse = interactor?.retrieveArticlesFromRealm()
                if (mArticleResponse.isNullOrEmpty())
                    RxUtils.manageExceptions(throwable, view)
                else
                    view.displayNewsFirstPage(articles = mArticleResponse)
                view.dismissLoadingIndicator()
            }
            AppConstants.LoadingMode.PAGINATION_LOADING -> view.hideLoadMoreIndicator()
            AppConstants.LoadingMode.KEYWORD_SEARCH -> {
                RxUtils.manageExceptions(throwable, view)
                view.hideLoadMoreIndicator()
            }
            AppConstants.LoadingMode.SWIPE_TO_REFRESH -> {
                val mArticleResponse = interactor?.retrieveArticlesFromRealm()
                if (mArticleResponse.isNullOrEmpty())
                    RxUtils.manageExceptions(throwable, view)
                else
                    view.displayNewsFirstPage(articles = mArticleResponse)
                view.dismissSwipeToRefreshIndicator()
            }
        }
    }
}