package al.marketlogic.news.ui.news.presenter

import al.marketlogic.news.ui.news.view.NewsViewHolderView
import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NewsViewHolderPresenter (private var newsAdapterPresenter: NewsAdapterPresenter) {

    fun clearViewHolder(viewHolderView: NewsViewHolderView) = viewHolderView.let {
        it.setTitle()
        it.setDate()
        it.loadThumbnail()
    }

    fun onBindViewHolder(position: Int, viewHolderView: NewsViewHolderView) = viewHolderView.let {
        val article = newsAdapterPresenter.getItemAt(position)

        it.setTitle(title = article.title)
        it.setDate(date = DateTime(article.publishedAt))
        it.loadThumbnail(thumbnailUrl = article.urlToImage)
        it.setOnClickListeners()
    }

    fun onClickListenerItem(position: Int) = newsAdapterPresenter.itemClickedAtPosition(position)
}