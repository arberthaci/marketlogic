package al.marketlogic.news.ui.news.presenter

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.base.presenter.MVPPresenter
import al.marketlogic.news.ui.news.interactor.NewsMVPInteractor
import al.marketlogic.news.ui.news.view.NewsMVPView

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface NewsMVPPresenter <V : NewsMVPView, I : NewsMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()

    fun onRefreshFeed(keyword: String)

    fun afterTextChanged(input: String)
    fun onClearSearchClicked()

    fun onRetrieveNewsFeed(page: Int, keyword: String)

    fun onArticleSelected(article: Article)
}