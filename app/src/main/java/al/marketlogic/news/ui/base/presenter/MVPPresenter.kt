package al.marketlogic.news.ui.base.presenter

import al.marketlogic.news.ui.base.interactor.MVPInteractor
import al.marketlogic.news.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?
}