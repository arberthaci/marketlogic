package al.marketlogic.news.ui.article.presenter

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.article.interactor.ArticleMVPInteractor
import al.marketlogic.news.ui.article.view.ArticleMVPView
import al.marketlogic.news.ui.base.presenter.MVPPresenter

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface ArticleMVPPresenter <V : ArticleMVPView, I : ArticleMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared(article: Article)

    fun onContinueReadingOnBrowserClicked()
}