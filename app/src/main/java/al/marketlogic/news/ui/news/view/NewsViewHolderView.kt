package al.marketlogic.news.ui.news.view

import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface NewsViewHolderView {

    fun setTitle(title: String? = null)
    fun setDate(date: DateTime? = null)
    fun loadThumbnail(thumbnailUrl: String? = null)

    fun setOnClickListeners()
}