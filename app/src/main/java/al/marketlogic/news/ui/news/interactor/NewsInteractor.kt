package al.marketlogic.news.ui.news.interactor

import al.marketlogic.news.data.db.DbOperations
import al.marketlogic.news.data.network.ApiHelper
import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.data.network.model.FeedResponse
import al.marketlogic.news.data.prefs.PreferenceHelper
import al.marketlogic.news.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NewsInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), NewsMVPInteractor {

    override fun callRetrieveTopHeadlines(filterParameters: Map<String, String>): Observable<FeedResponse> =
            apiHelper.provideTopHeadlinesService().getTopHeadlines(filterParameters = filterParameters)

    override fun insertArticlesInRealm(articles: List<Article>) =
            dbOperations.insertArticles(articles = articles)

    override fun retrieveArticlesFromRealm(): List<Article> =
            dbOperations.retrieveAllArticles()

    override fun deleteArticlesFromRealm() =
            dbOperations.deleteAllArticles()
}