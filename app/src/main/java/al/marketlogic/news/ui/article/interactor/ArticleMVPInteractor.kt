package al.marketlogic.news.ui.article.interactor

import al.marketlogic.news.ui.base.interactor.MVPInteractor

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface ArticleMVPInteractor : MVPInteractor