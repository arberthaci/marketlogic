package al.marketlogic.news.ui.news.presenter

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.news.view.NewsAdapterView
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NewsAdapterPresenter @Inject internal constructor(private val newsListItems: MutableList<Article>) {

    var view: NewsAdapterView? = null

    fun getItemCount() =
            this.newsListItems.size

    fun getItemAt(position: Int) =
            this.newsListItems[position]

    fun insertNewsToList(articles: List<Article>) {
        clearList()
        loadMoreNewsToList(articles)
    }

    fun loadMoreNewsToList(articles: List<Article>) {
        this.newsListItems.addAll(articles)
        notifyDataSetChanged()
    }

    fun clearList() {
        this.newsListItems.clear()
        notifyDataSetChanged()
    }

    fun notifyDataSetChanged() =
            view?.adapterNotifyDataSetChanged()

    fun itemClickedAtPosition(position: Int) =
            view?.adapterItemClicked(getItemAt(position))
}