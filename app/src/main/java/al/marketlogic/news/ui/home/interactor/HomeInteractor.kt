package al.marketlogic.news.ui.home.interactor

import al.marketlogic.news.data.db.DbOperations
import al.marketlogic.news.data.network.ApiHelper
import al.marketlogic.news.data.prefs.PreferenceHelper
import al.marketlogic.news.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class HomeInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), HomeMVPInteractor