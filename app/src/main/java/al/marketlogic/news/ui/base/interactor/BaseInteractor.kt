package al.marketlogic.news.ui.base.interactor

import al.marketlogic.news.data.db.DbOperations
import al.marketlogic.news.data.network.ApiHelper
import al.marketlogic.news.data.prefs.PreferenceHelper

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

open class BaseInteractor() : MVPInteractor {

    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var apiHelper: ApiHelper
    protected lateinit var dbOperations: DbOperations

    constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : this() {
        this.preferenceHelper = preferenceHelper
        this.apiHelper = apiHelper
        this.dbOperations = dbOperations
    }
}