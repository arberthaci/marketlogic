package al.marketlogic.news.ui.article.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import al.marketlogic.news.R
import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.article.interactor.ArticleMVPInteractor
import al.marketlogic.news.ui.article.presenter.ArticleMVPPresenter
import al.marketlogic.news.ui.base.view.BaseFragment
import al.marketlogic.news.utils.extensions.loadImage
import al.marketlogic.news.utils.extensions.validatePhotoUrl
import kotlinx.android.synthetic.main.fragment_article.*
import javax.inject.Inject
import android.content.Intent
import android.net.Uri


/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class ArticleFragment : BaseFragment(), ArticleMVPView {

    @Inject
    internal lateinit var presenter: ArticleMVPPresenter<ArticleMVPView, ArticleMVPInteractor>

    var article: Article? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_article, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        b_article_continue_reading_on_browser.setOnClickListener { presenter.onContinueReadingOnBrowserClicked() }

        article?.let { presenter.onViewPrepared(article = it) }
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * ArticleMVPView implementation
     * Start
     **********************************
     **/


    override fun loadArticlePhoto(photo: String?) {
        iv_article_photo.loadImage(photo.validatePhotoUrl())
    }

    override fun displayArticleTitle(title: String?) {
        title?.let { tv_article_title.text = it }
    }

    override fun displayArticleSourceAndDate(source: String?, date: String, time: String) {
        source?.let { mSource ->
            tv_article_source_date.text = getString(R.string.article_source_date_placeholder, mSource, date, time)
        }
    }

    override fun displayArticleContent(content: String?) {
        content?.let { tv_article_content.text = it }
    }

    override fun openBrowserIntent(url: String?) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    /**
     **********************************
     * ArticleMVPView implementation
     * End
     **********************************
     **/

    companion object {
        internal val TAG = "article_ft"
        internal val TITLE = "Reading article"

        fun newInstance(): ArticleFragment {
            return ArticleFragment()
        }
    }
}
