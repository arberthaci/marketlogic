package al.marketlogic.news.ui.news.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import al.marketlogic.news.R
import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.article.view.ArticleFragment
import al.marketlogic.news.ui.base.view.BaseFragment
import al.marketlogic.news.ui.news.interactor.NewsMVPInteractor
import al.marketlogic.news.ui.news.presenter.NewsMVPPresenter
import al.marketlogic.news.utils.AppConstants
import al.marketlogic.news.utils.customviews.ErrorViewState
import al.marketlogic.news.utils.extensions.convertDpToPixel
import al.marketlogic.news.utils.extensions.hide
import al.marketlogic.news.utils.extensions.show
import al.marketlogic.news.utils.itemdecorations.DividerItemDecoration
import al.marketlogic.news.utils.rvadapterlisteners.EndlessRecyclerViewScrollListener
import al.marketlogic.news.utils.rvadapterlisteners.OnNewsActionListener
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_news.*
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NewsFragment : BaseFragment(), NewsMVPView, OnNewsActionListener {

    @Inject
    internal lateinit var newsAdapter: NewsAdapter
    @Inject
    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var presenter: NewsMVPPresenter<NewsMVPView, NewsMVPInteractor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        prepareSwipeToRefresh()
        prepareSearchBar()
        prepareRecyclerView()

        presenter.onViewPrepared()
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * NewsMVPView implementation
     * Start
     **********************************
     **/

    override fun idleSearchIcon() {
        iv_news_search.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_search_grey_700_24dp))
    }

    override fun activateSearchIcon() {
        iv_news_search.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_search_primary_24dp))
    }

    override fun hideClearIcon() {
        iv_news_clear_search.hide()
    }

    override fun showClearIcon() {
        iv_news_clear_search.show()
    }

    override fun clearSearchInput() {
        et_news_search.text.clear()
    }

    override fun prepareLoading() {
        showLoadingIndicator()
        ev_news.setState(ErrorViewState.NO_ERROR)
        rv_news.hide()
        newsAdapter.presenter.clearList()
    }

    override fun showLoadMoreIndicator() {
        pb_load_more.show()
    }

    override fun hideLoadMoreIndicator() {
        pb_load_more.isInvisible = true
    }

    override fun dismissSwipeToRefreshIndicator() {
        srl_news.isRefreshing = false
    }

    override fun displayNewsFirstPage(articles: List<Article>?) = articles?.let {
        newsAdapter.presenter.insertNewsToList(articles = it)
        ev_news.setState(ErrorViewState.NO_ERROR)
        rv_news.show()
    }

    override fun displayNewsNextPage(articles: List<Article>?) = articles?.let {
        newsAdapter.presenter.loadMoreNewsToList(articles = it)
    }

    override fun showErrorView(state: ErrorViewState) {
        rv_news.hide()
        ev_news.setState(state)
    }

    override fun openArticleFragment(article: Article) {
        val articleFragment = ArticleFragment.newInstance()
        articleFragment.article = article

        getBaseActivity()?.openNewFragment(
                fragment = articleFragment,
                tag = ArticleFragment.TAG,
                title = ArticleFragment.TITLE,
                fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_SLIDE)
    }

    /**
     **********************************
     * NewsMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * OnCategoryActionListener implementation
     * Start
     **********************************
     **/

    override fun onItemSelected(article: Article) =
            presenter.onArticleSelected(article = article)

    /**
     **********************************
     * OnCategoryActionListener implementation
     * End
     **********************************
     **/

    private fun prepareSwipeToRefresh() {
        srl_news.setColorSchemeResources(R.color.colorAccent)
        srl_news.setOnRefreshListener { presenter.onRefreshFeed(keyword = et_news_search.text.toString()) }
    }

    private fun prepareSearchBar() {
        et_news_search.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {
                presenter.afterTextChanged(input = s.toString())
            }
        })

        iv_news_clear_search.setOnClickListener { presenter.onClearSearchClicked() }
    }

    private fun prepareRecyclerView() {
        layoutManager.orientation = RecyclerView.VERTICAL
        rv_news.layoutManager = layoutManager
        rv_news.itemAnimator = DefaultItemAnimator()

        context?.let { mContext ->
            val divider = ContextCompat.getDrawable(mContext, R.drawable.divider)
            divider?.let { mDivider ->
                rv_news.addItemDecoration(DividerItemDecoration(divider = mDivider, marginLeft = 141f.convertDpToPixel(context = mContext)))
            }
        }

        val scrollListener: EndlessRecyclerViewScrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, recyclerView: RecyclerView?) {
                presenter.onRetrieveNewsFeed(page = page + 1, keyword = et_news_search.text.toString())
            }
        }

        rv_news.addOnScrollListener(scrollListener)
        rv_news.adapter = newsAdapter
        newsAdapter.onNewsActionListener = this
    }

    companion object {
        internal val TAG = "news_ft"
        internal val TITLE = "News Feed"

        fun newInstance(): NewsFragment {
            return NewsFragment()
        }
    }
}
