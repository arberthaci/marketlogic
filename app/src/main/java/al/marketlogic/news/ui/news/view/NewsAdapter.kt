package al.marketlogic.news.ui.news.view

import al.marketlogic.news.R
import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.news.presenter.NewsAdapterPresenter
import al.marketlogic.news.ui.news.presenter.NewsViewHolderPresenter
import al.marketlogic.news.utils.AppConstants
import al.marketlogic.news.utils.extensions.loadImage
import al.marketlogic.news.utils.extensions.toString
import al.marketlogic.news.utils.rvadapterlisteners.OnNewsActionListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_news_list.view.*
import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class NewsAdapter (val presenter: NewsAdapterPresenter)
    : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>(), NewsAdapterView {

    init {
        presenter.view = this
    }

    lateinit var onNewsActionListener: OnNewsActionListener

    override fun getItemCount() = this.presenter.getItemCount()

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) = holder.let {
        holder.viewHolderPresenter.clearViewHolder(it)
        holder.viewHolderPresenter.onBindViewHolder(position, it)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_news_list, parent, false))

    override fun adapterNotifyDataSetChanged() {
        notifyDataSetChanged()
    }

    override fun adapterItemClicked(article: Article) =
            onNewsActionListener.onItemSelected(article = article)

    inner class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view), NewsViewHolderView {

        val viewHolderPresenter = NewsViewHolderPresenter(presenter)

        override fun setTitle(title: String?) {
            title?.let { itemView.tv_single_news_title.text = it }
                    ?: run { itemView.tv_single_news_title.text = "" }
        }

        override fun setDate(date: DateTime?) {
            date?.let {
                val dateString = it.toString(AppConstants.DateStringMode.DATE_STRING_WITHOUT_YEAR_MODE)
                val timeString = it.toString(AppConstants.DateStringMode.TIME_MODE)
                itemView.tv_single_news_date.text = itemView.context.getString(R.string.news_item_date_placeholder, dateString, timeString)
            } ?: run {
                itemView.tv_single_news_date.text = ""
            }
        }

        override fun loadThumbnail(thumbnailUrl: String?) {
            thumbnailUrl?.let { itemView.iv_single_news_thumbnail.loadImage(it) }
                    ?: run { itemView.iv_single_news_thumbnail.setImageDrawable(null) }
        }

        override fun setOnClickListeners() {
            if (adapterPosition != RecyclerView.NO_POSITION)
                itemView.rl_single_news_item.setOnClickListener { viewHolderPresenter.onClickListenerItem(adapterPosition) }
        }
    }
}