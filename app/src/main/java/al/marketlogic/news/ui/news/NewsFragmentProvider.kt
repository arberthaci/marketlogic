package al.marketlogic.news.ui.news

import al.marketlogic.news.ui.news.view.NewsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class NewsFragmentProvider {

    @ContributesAndroidInjector(modules = [NewsFragmentModule::class])
    internal abstract fun provideNewsFragmentFactory(): NewsFragment
}