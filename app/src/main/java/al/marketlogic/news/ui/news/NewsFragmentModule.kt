package al.marketlogic.news.ui.news

import al.marketlogic.news.ui.news.interactor.NewsInteractor
import al.marketlogic.news.ui.news.interactor.NewsMVPInteractor
import al.marketlogic.news.ui.news.presenter.NewsAdapterPresenter
import al.marketlogic.news.ui.news.presenter.NewsMVPPresenter
import al.marketlogic.news.ui.news.presenter.NewsPresenter
import al.marketlogic.news.ui.news.view.NewsAdapter
import al.marketlogic.news.ui.news.view.NewsFragment
import al.marketlogic.news.ui.news.view.NewsMVPView
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
class NewsFragmentModule {

    @Provides
    internal fun provideNewsInteractor(interactor: NewsInteractor): NewsMVPInteractor = interactor

    @Provides
    internal fun provideNewsPresenter(presenter: NewsPresenter<NewsMVPView, NewsMVPInteractor>)
            : NewsMVPPresenter<NewsMVPView, NewsMVPInteractor> = presenter

    @Provides
    internal fun provideNewsAdapterPresenter(interactor: NewsInteractor): NewsAdapterPresenter = NewsAdapterPresenter(arrayListOf())

    @Provides
    internal fun provideNewsAdapter(presenter: NewsAdapterPresenter): NewsAdapter = NewsAdapter(presenter)

    @Provides
    internal fun provideLinearLayoutManager(fragment: NewsFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)
}