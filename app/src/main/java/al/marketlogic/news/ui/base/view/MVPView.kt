package al.marketlogic.news.ui.base.view

import al.marketlogic.news.utils.AppConstants
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface MVPView {

    fun showLoadingIndicator()

    fun dismissLoadingIndicator()

    fun showAlerter(alerterMode: AppConstants.AlerterMode, message: String)

    fun isNetworkConnected(): Observable<Boolean>

    fun hasNetworkConnection(): Boolean
}