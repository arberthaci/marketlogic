package al.marketlogic.news.ui.home.view

import al.marketlogic.news.ui.base.view.MVPView
import androidx.fragment.app.Fragment

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPView : MVPView {

    fun setToolbarTitle(title: String)

    fun getCurrentFragment(): Fragment?
    fun openNewsFragment()
    fun closeArticleFragment()

    fun invalidateOptionsMenuEvent(): Unit?

    fun showBackButton()
    fun hideBackButton()
}