package al.marketlogic.news.ui.news.interactor

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.data.network.model.FeedResponse
import al.marketlogic.news.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface NewsMVPInteractor : MVPInteractor {

    fun callRetrieveTopHeadlines(filterParameters: Map<String, String>): Observable<FeedResponse>

    fun insertArticlesInRealm(articles: List<Article>)
    fun retrieveArticlesFromRealm(): List<Article>
    fun deleteArticlesFromRealm()
}