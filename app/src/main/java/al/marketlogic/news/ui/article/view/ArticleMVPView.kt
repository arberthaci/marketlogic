package al.marketlogic.news.ui.article.view

import al.marketlogic.news.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface ArticleMVPView : MVPView {

    fun loadArticlePhoto(photo: String?)
    fun displayArticleTitle(title: String?)
    fun displayArticleSourceAndDate(source: String?, date: String, time: String)
    fun displayArticleContent(content: String?)

    fun openBrowserIntent(url: String?)
}