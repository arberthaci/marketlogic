package al.marketlogic.news.ui.news.view

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.base.view.MVPView
import al.marketlogic.news.utils.customviews.ErrorViewState

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface NewsMVPView : MVPView {

    fun idleSearchIcon()
    fun activateSearchIcon()
    fun hideClearIcon()
    fun showClearIcon()
    fun clearSearchInput()

    fun prepareLoading()
    fun showLoadMoreIndicator()
    fun hideLoadMoreIndicator()
    fun dismissSwipeToRefreshIndicator()
    fun displayNewsFirstPage(articles: List<Article>?): Unit?
    fun displayNewsNextPage(articles: List<Article>?): Unit?
    fun showErrorView(state: ErrorViewState)

    fun openArticleFragment(article: Article)
}