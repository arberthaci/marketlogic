package al.marketlogic.news.ui.article.presenter

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.ui.article.interactor.ArticleMVPInteractor
import al.marketlogic.news.ui.article.view.ArticleMVPView
import al.marketlogic.news.ui.base.presenter.BasePresenter
import al.marketlogic.news.utils.AppConstants
import al.marketlogic.news.utils.extensions.toString
import al.marketlogic.news.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import org.joda.time.DateTime
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class ArticlePresenter <V : ArticleMVPView, I : ArticleMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), ArticleMVPPresenter<V, I> {

    lateinit var article: Article

    override fun onViewPrepared(article: Article) {
        this.article = article

        getView()?.loadArticlePhoto(photo = article.urlToImage)
        getView()?.displayArticleTitle(title = article.title)
        getView()?.displayArticleSourceAndDate(source = article.source?.name,
                date = DateTime(article.publishedAt).toString(AppConstants.DateStringMode.DATE_STRING_WITHOUT_YEAR_MODE) ?: "",
                time = DateTime(article.publishedAt).toString(AppConstants.DateStringMode.TIME_MODE) ?: "")
        getView()?.displayArticleContent(content = article.content)
    }

    override fun onContinueReadingOnBrowserClicked() {
        getView()?.openBrowserIntent(url = article.url)
    }
}