package al.marketlogic.news.ui.news.view

import al.marketlogic.news.data.network.model.Article

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface NewsAdapterView {

    fun adapterNotifyDataSetChanged()
    fun adapterItemClicked(article: Article)
}