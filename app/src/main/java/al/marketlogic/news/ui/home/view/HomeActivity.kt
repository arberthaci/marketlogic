package al.marketlogic.news.ui.home.view

import al.marketlogic.news.R
import al.marketlogic.news.ui.article.view.ArticleFragment
import al.marketlogic.news.ui.base.view.BaseActivity
import al.marketlogic.news.ui.home.interactor.HomeMVPInteractor
import al.marketlogic.news.ui.home.presenter.HomeMVPPresenter
import al.marketlogic.news.ui.news.view.NewsFragment
import al.marketlogic.news.utils.AppConstants
import al.marketlogic.news.utils.extensions.addFragment
import al.marketlogic.news.utils.extensions.getCurrentFragment
import al.marketlogic.news.utils.extensions.removeFragment
import android.os.Bundle
import androidx.fragment.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class HomeActivity : BaseActivity(), HomeMVPView, HasSupportFragmentInjector {

    @Inject
    internal lateinit var presenter: HomeMVPPresenter<HomeMVPView, HomeMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onBackPressed() {
        presenter.onBackPressed()?.let {
            if (it) super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        presenter.onBackPressed()?.let {
            if (it) super.onBackPressed()
        }

        return true
    }

    /**
     **********************************
     * HomeMVPView implementation
     * Start
     **********************************
     **/

    override fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun getCurrentFragment(): Fragment? = supportFragmentManager?.getCurrentFragment(R.id.home_fragment_container)

    override fun openNewsFragment() = openNewFragment(fragment = NewsFragment.newInstance(),
            tag = NewsFragment.TAG,
            title = NewsFragment.TITLE,
            fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_FADE)

    override fun closeArticleFragment() = onFragmentDetached(tag = ArticleFragment.TAG, fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_SLIDE)

    override fun invalidateOptionsMenuEvent(): Unit? = invalidateOptionsMenu()

    override fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun hideBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    /**
     **********************************
     * HomeMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * Start
     **********************************
     **/

    override fun onFragmentAttached() {}

    override fun onFragmentResumed() = presenter.onFragmentResumed()

    override fun onFragmentDetached(tag: String, fragmentAnimationMode: AppConstants.FragmentAnimationMode, detachInBackground: Boolean) {
        supportFragmentManager?.removeFragment(tag = tag, fragmentAnimationMode = fragmentAnimationMode)
        if (!detachInBackground) onFragmentResumed()
    }

    override fun openNewFragment(fragment: Fragment, tag: String, title: String?, fragmentAnimationMode: AppConstants.FragmentAnimationMode) {
        supportFragmentManager.addFragment(containerViewId = R.id.home_fragment_container,
                fragment = fragment,
                tag = tag,
                fragmentAnimationMode = fragmentAnimationMode)
        title?.let { setToolbarTitle(it) }
    }

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * End
     **********************************
     **/
}
