package al.marketlogic.news.ui.article

import al.marketlogic.news.ui.article.interactor.ArticleInteractor
import al.marketlogic.news.ui.article.interactor.ArticleMVPInteractor
import al.marketlogic.news.ui.article.presenter.ArticleMVPPresenter
import al.marketlogic.news.ui.article.presenter.ArticlePresenter
import al.marketlogic.news.ui.article.view.ArticleMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
class ArticleFragmentModule {

    @Provides
    internal fun provideArticleInteractor(interactor: ArticleInteractor): ArticleMVPInteractor = interactor

    @Provides
    internal fun provideArticlePresenter(presenter: ArticlePresenter<ArticleMVPView, ArticleMVPInteractor>)
            : ArticleMVPPresenter<ArticleMVPView, ArticleMVPInteractor> = presenter
}