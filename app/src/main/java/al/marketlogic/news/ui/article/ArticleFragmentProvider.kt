package al.marketlogic.news.ui.article

import al.marketlogic.news.ui.article.view.ArticleFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class ArticleFragmentProvider {

    @ContributesAndroidInjector(modules = [ArticleFragmentModule::class])
    internal abstract fun provideArticleFragmentFactory(): ArticleFragment
}