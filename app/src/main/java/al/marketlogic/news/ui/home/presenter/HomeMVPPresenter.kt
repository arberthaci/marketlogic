package al.marketlogic.news.ui.home.presenter

import al.marketlogic.news.ui.base.presenter.MVPPresenter
import al.marketlogic.news.ui.home.interactor.HomeMVPInteractor
import al.marketlogic.news.ui.home.view.HomeMVPView
import android.view.Menu

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPPresenter<V : HomeMVPView, I : HomeMVPInteractor> : MVPPresenter<V, I> {

    fun onFragmentResumed()

    fun onBackPressed(): Boolean?
}