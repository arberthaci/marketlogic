package al.marketlogic.news.ui.home

import al.marketlogic.news.ui.home.interactor.HomeInteractor
import al.marketlogic.news.ui.home.interactor.HomeMVPInteractor
import al.marketlogic.news.ui.home.presenter.HomeMVPPresenter
import al.marketlogic.news.ui.home.presenter.HomePresenter
import al.marketlogic.news.ui.home.view.HomeMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
class HomeActivityModule {

    @Provides
    internal fun provideHomeInteractor(homeInteractor: HomeInteractor): HomeMVPInteractor = homeInteractor

    @Provides
    internal fun provideHomePresenter(homePresenter: HomePresenter<HomeMVPView, HomeMVPInteractor>)
            : HomeMVPPresenter<HomeMVPView, HomeMVPInteractor> = homePresenter
}