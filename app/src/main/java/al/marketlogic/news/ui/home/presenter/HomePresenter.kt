package al.marketlogic.news.ui.home.presenter

import al.marketlogic.news.ui.article.view.ArticleFragment
import al.marketlogic.news.ui.base.presenter.BasePresenter
import al.marketlogic.news.ui.home.interactor.HomeMVPInteractor
import al.marketlogic.news.ui.home.view.HomeMVPView
import al.marketlogic.news.ui.news.view.NewsFragment
import al.marketlogic.news.utils.rx.SchedulerProvider
import androidx.fragment.app.Fragment
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class HomePresenter<V : HomeMVPView, I : HomeMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), HomeMVPPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        getView()?.openNewsFragment()
    }

    override fun onFragmentResumed() {
        when(getCurrentFragment()) {
            is NewsFragment -> getView()?.let {
                it.hideBackButton()
                it.setToolbarTitle(NewsFragment.TITLE)
            }
            is ArticleFragment -> getView()?.let {
                it.showBackButton()
                it.setToolbarTitle(ArticleFragment.TITLE)
            }
        }

        getView()?.invalidateOptionsMenuEvent()
    }

    override fun onBackPressed(): Boolean? {
        when (getCurrentFragment()) {
            is NewsFragment -> return true
            is ArticleFragment -> getView()?.closeArticleFragment()
        }
        return null
    }

    private fun getCurrentFragment(): Fragment? = getView()?.getCurrentFragment()
}