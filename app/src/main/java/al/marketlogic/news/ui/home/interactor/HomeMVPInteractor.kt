package al.marketlogic.news.ui.home.interactor

import al.marketlogic.news.ui.base.interactor.MVPInteractor

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPInteractor : MVPInteractor