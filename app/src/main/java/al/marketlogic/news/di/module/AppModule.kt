package al.marketlogic.news.di.module

import al.marketlogic.news.data.db.DbMigration
import al.marketlogic.news.data.db.DbOperations
import al.marketlogic.news.data.db.article.ArticleHelper
import al.marketlogic.news.data.db.article.ArticleRepository
import al.marketlogic.news.data.network.ApiHelper
import al.marketlogic.news.data.prefs.AppPreferenceHelper
import al.marketlogic.news.data.prefs.PreferenceHelper
import al.marketlogic.news.di.PreferenceInfo
import al.marketlogic.news.utils.AppConstants
import al.marketlogic.news.utils.rx.SchedulerProvider
import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideRealmDatabase(context: Context): Realm {
        Realm.init(context)
        return Realm.getInstance(RealmConfiguration.Builder()
                .name("realm.marketlogic.news")
                .schemaVersion(1)
                .migration(DbMigration())
                .build())
    }

    @Provides
    @PreferenceInfo
    internal fun providePrefFileName(): String = AppConstants.SHARED_PREFS_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideApiHelper(): ApiHelper = ApiHelper()

    @Provides
    @Singleton
    internal fun provideDbOperations(articleHelper: ArticleHelper): DbOperations = DbOperations(articleHelper = articleHelper)

    @Provides
    @Singleton
    internal fun provideArticleHelper(realm: Realm): ArticleHelper = ArticleRepository(realm)

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()
}