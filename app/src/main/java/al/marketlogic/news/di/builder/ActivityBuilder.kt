package al.marketlogic.news.di.builder

import al.marketlogic.news.ui.article.ArticleFragmentProvider
import al.marketlogic.news.ui.home.HomeActivityModule
import al.marketlogic.news.ui.home.view.HomeActivity
import al.marketlogic.news.ui.news.NewsFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(HomeActivityModule::class), (NewsFragmentProvider::class), (ArticleFragmentProvider::class)])

    abstract fun bindHomeActivity(): HomeActivity
}