package al.marketlogic.news.di

import javax.inject.Qualifier

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

@Qualifier
@Retention annotation class PreferenceInfo