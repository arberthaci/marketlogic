package al.marketlogic.news.data.db.article

import al.marketlogic.news.data.network.model.Article

/**
 * Created by Arbër Thaçi on 19-06-05.
 * Email: arberlthaci@gmail.com
 */

interface ArticleHelper {

    fun insertArticles(articles: List<Article>)

    fun retrieveAllArticles(): List<Article>

    fun deleteAllArticles()
}