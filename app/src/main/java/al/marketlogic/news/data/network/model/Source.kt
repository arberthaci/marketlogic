package al.marketlogic.news.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

open class Source(@Expose
                  @SerializedName("id")
                  var id: String? = null,

                  @Expose
                  @SerializedName("name")
                  var name: String? = null): RealmObject()