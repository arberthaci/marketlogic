package al.marketlogic.news.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import java.util.*

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

open class Article(@Expose
                   @SerializedName("source")
                   var source: Source? = null,

                   @Expose
                   @SerializedName("author")
                   var author: String? = null,

                   @Expose
                   @SerializedName("title")
                   var title: String? = null,

                   @Expose
                   @SerializedName("description")
                   var description: String? = null,

                   @Expose
                   @SerializedName("url")
                   var url: String? = null,

                   @Expose
                   @SerializedName("urlToImage")
                   var urlToImage: String? = null,

                   @Expose
                   @SerializedName("publishedAt")
                   var publishedAt: Date? = null,

                   @Expose
                   @SerializedName("content")
                   var content: String? = null): RealmObject()