package al.marketlogic.news.data.network

import al.marketlogic.news.data.network.model.FeedResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

interface TopHeadlinesApi {

    @GET(ApiEndPoint.ENDPOINT_TOP_HEADLINES)
    fun getTopHeadlines(@QueryMap filterParameters: Map<String, String> = mapOf()): Observable<FeedResponse>
}