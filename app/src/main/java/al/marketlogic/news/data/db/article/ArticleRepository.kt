package al.marketlogic.news.data.db.article

import al.marketlogic.news.data.network.model.Article
import io.realm.Realm
import io.realm.kotlin.delete
import io.realm.kotlin.where
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-05.
 * Email: arberlthaci@gmail.com
 */

class ArticleRepository @Inject constructor(private val realm: Realm): ArticleHelper {

    override fun insertArticles(articles: List<Article>) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { mRealm ->
                mRealm.insertOrUpdate(articles)
            }
        }
    }

    override fun retrieveAllArticles(): List<Article> {
        return Realm.getDefaultInstance().use { realm ->
            realm.copyFromRealm(realm.where<Article>()
                    .findAll())
        }
    }

    override fun deleteAllArticles() {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { mRealm ->
                mRealm.delete<Article>()
            }
        }
    }
}