package al.marketlogic.news.data.network

import al.marketlogic.news.BuildConfig

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

object ApiEndPoint {

    private const val API_POSTFIX = "v2/"

    const val ENDPOINT_TOP_HEADLINES = BuildConfig.BASE_URL + API_POSTFIX + "top-headlines/"
}