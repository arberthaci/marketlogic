package al.marketlogic.news.data.prefs

import al.marketlogic.news.di.PreferenceInfo
import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class AppPreferenceHelper @Inject constructor(context: Context, @PreferenceInfo private val prefFileName: String) : PreferenceHelper {

    companion object {
        private const val PREF_KEY = "PREF_KEY"
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
}