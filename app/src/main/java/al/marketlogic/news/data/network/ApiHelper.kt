package al.marketlogic.news.data.network

import al.marketlogic.news.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class ApiHelper {

    private fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor { chain ->
                    val original = chain.request()
                    val originalHttpUrl = original.url()

                    val url = originalHttpUrl.newBuilder()
                            .addQueryParameter("apiKey", BuildConfig.AUTHORIZATION_KEY)
                            .build()

                    val requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .url(url)

                    val request = requestBuilder.build()
                    chain.proceed(request)
                }
                .build()
    }

    private fun provideRetrofit(): Retrofit {
        val gsonBuilder = GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .excludeFieldsWithoutExposeAnnotation()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(provideHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    fun provideTopHeadlinesService(): TopHeadlinesApi =
            provideRetrofit().create(TopHeadlinesApi::class.java)
}