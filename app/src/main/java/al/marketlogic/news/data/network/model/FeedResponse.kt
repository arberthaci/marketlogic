package al.marketlogic.news.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

data class FeedResponse(@Expose
                        @SerializedName("status")
                        var status: String? = null,

                        @Expose
                        @SerializedName("totalResults")
                        var totalResults: Int? = 0,

                        @Expose
                        var articles: List<Article>? = null)