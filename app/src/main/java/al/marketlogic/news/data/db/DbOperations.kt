package al.marketlogic.news.data.db

import al.marketlogic.news.data.db.article.ArticleHelper
import al.marketlogic.news.data.network.model.Article
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 19-06-03.
 * Email: arberlthaci@gmail.com
 */

class DbOperations @Inject internal constructor(private var articleHelper: ArticleHelper) : ArticleHelper {

    override fun insertArticles(articles: List<Article>) =
            articleHelper.insertArticles(articles =  articles)

    override fun retrieveAllArticles(): List<Article> =
            articleHelper.retrieveAllArticles()

    override fun deleteAllArticles() =
            articleHelper.deleteAllArticles()
}