package al.marketlogic.news

import al.marketlogic.news.data.network.ApiHelper
import al.marketlogic.news.data.network.model.FeedResponse
import com.google.gson.Gson
import io.reactivex.observers.TestObserver
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.HashMap

/**
 * Created by Arbër Thaçi on 19-06-06.
 * Email: arberlthaci@gmail.com
 */

class TopHeadlinesApiTest {

    private lateinit var gson: Gson
    private lateinit var mockWebServer: MockWebServer
    private lateinit var testObserver: TestObserver<FeedResponse>

    private val goodResponseJson = "{\"status\":\"ok\",\"totalResults\":68,\"articles\":[{\"source\":{\"name\":\"Thesun.co.uk\"},\"author\":\"SunSport Reporters\",\"title\":\"9.30am Chelsea transfer news: Hazard £88.5m bid accepted, Javi Gracia latest, Hudson-Odoi to stay - The Sun\",\"description\":\"\",\"url\":\"https://www.thesun.co.uk/sport/football/9206116/chelsea-transfer-news-hazard-hudson-odoi-sarri-kovacic/\",\"urlToImage\":\"https://www.thesun.co.uk/wp-content/uploads/2019/06/hazard-bid.jpg?strip\\u003dall\\u0026quality\\u003d100\\u0026w\\u003d1200\\u0026h\\u003d800\\u0026crop\\u003d1\",\"publishedAt\":\"Jun 6, 2019 10:44:14 AM\",\"content\":\"WATFORD believe Javi Gracia will stay in charge of the club despite interest from Chelsea, it has been claimed.\\r\\nGracia has been linked with a move to Stamford Bridge this summer if Maurizio Sarri quits to join Juventus.\\r\\nThe Blues have been looking out for a… [+901 chars]\"},{\"source\":{\"name\":\"Express.co.uk\"},\"author\":\"Jack Otway\",\"title\":\"Liverpool boss Jurgen Klopp wants SABBATICAL and sets deadline for Anfield exit - Express\",\"description\":\"LIVERPOOL boss Jurgen Klopp is ready to leave the Reds in 2022 in order to take a sabbatical, according to reports.\",\"url\":\"https://www.express.co.uk/sport/football/1137018/Liverpool-Jurgen-Klopp-Premier-League-news\",\"urlToImage\":\"https://cdn.images.express.co.uk/img/dynamic/67/750x445/1137018.jpg\",\"publishedAt\":\"Jun 6, 2019 10:25:00 AM\"}]}"
    private val badResponseJson = "{\"status\":\"false\",\"totalResults\":2,\"articles\":[{\"source\":{\"name\": 0},\"author\":\"SunSport Reporters\",\"title\":\"9.30am Chelsea transfer news\",\"description\":\"\",\"url\":\"https://www.thesun.co.uk/sport/football/9206116/chelsea-transfer-news-hazard-hudson-odoi-sarri-kovacic/\"]}"

    @Before
    fun setUp() {
        gson = Gson()
        mockWebServer = MockWebServer()
        testObserver = TestObserver()
    }

    /**
     * In this use case,
     * parsing of the response
     * SUCCESSFULLY passes the test
     **/
    @Test
    fun serverCallWithSuccessfulParsing() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson)
        mockWebServer.enqueue(mockResponse)

        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["pageSize"] = "2"
        ApiHelper().provideTopHeadlinesService().getTopHeadlines(filterParameters = filters).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        Assert.assertEquals(goodResponseJson, gson.toJson(testObserver.values()[0]))
    }

    /**
     * In this use case,
     * parsing of the invalid response
     * SUCCESSFULLY passes the assertNotEquals test
     **/
    @Test
    fun serverCallWithErrorParsing() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(badResponseJson)
        mockWebServer.enqueue(mockResponse)

        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["pageSize"] = "2"
        ApiHelper().provideTopHeadlinesService().getTopHeadlines(filterParameters = filters).subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertComplete()
        Assert.assertNotEquals(badResponseJson, gson.toJson(testObserver.values()[0]))
    }

    /**
     * In this use case,
     * handling the 200 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallForTestingEndpoint() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(goodResponseJson)
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["pageSize"] = "2"
        ApiHelper().provideTopHeadlinesService().getTopHeadlines(filterParameters = filters).subscribe(testObserver)

        Assert.assertEquals("HTTP/1.1 200 OK", mockResponse.status)

        mockWebServer.shutdown()
    }

    /**
     * In this use case,
     * handling the 404 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallWith404ResponseCode() {
        val mockResponse = MockResponse()
                .setResponseCode(404)
                .setBody("{}")
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["pageSize"] = "2"
        ApiHelper().provideTopHeadlinesService().getTopHeadlines(filterParameters = filters).subscribe(testObserver)

        Assert.assertEquals("HTTP/1.1 404 Client Error", mockResponse.status)

        mockWebServer.shutdown()
    }

    /**
     * In this use case,
     * handling the 500 response code
     * SUCCESSFULLY passes the test
     **/
    @Test
    @Throws(Exception::class)
    fun serverCallWith500ResponseCode() {
        val mockResponse = MockResponse()
                .setResponseCode(500)
                .setBody("{}")
        mockWebServer.enqueue(mockResponse)
        mockWebServer.start()

        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["pageSize"] = "2"
        ApiHelper().provideTopHeadlinesService().getTopHeadlines(filterParameters = filters).subscribe(testObserver)

        Assert.assertEquals("HTTP/1.1 500 Server Error", mockResponse.status)

        mockWebServer.shutdown()
    }
}