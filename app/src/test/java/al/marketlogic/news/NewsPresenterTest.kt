package al.marketlogic.news

import al.marketlogic.news.data.network.model.Article
import al.marketlogic.news.data.network.model.FeedResponse
import al.marketlogic.news.ui.news.interactor.NewsMVPInteractor
import al.marketlogic.news.ui.news.presenter.NewsPresenter
import al.marketlogic.news.ui.news.view.NewsMVPView
import al.marketlogic.news.utils.customviews.ErrorViewState
import al.marketlogic.news.utils.rx.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import java.util.HashMap

/**
 * Created by Arbër Thaçi on 19-06-06.
 * Email: arberlthaci@gmail.com
 */

class NewsPresenterTest {

    @Rule
    @JvmField var testSchedulerRule = RxImmediateSchedulerRule()

    private val view = mock(NewsMVPView::class.java)
    private val interactor = mock(NewsMVPInteractor::class.java)

    private lateinit var presenter: NewsPresenter<NewsMVPView, NewsMVPInteractor>

    @Before
    fun setUp() {
        presenter = NewsPresenter(interactor, SchedulerProvider(), CompositeDisposable())
        presenter.onAttach(view)
    }

    /**
     * In these tests
     * we're covering only the
     * `onViewPrepared()` cases
     **/

    @Test
    fun onViewPrepared_noInternetNoCachedFeeds_noInternetDisplayed() {
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(false))
        val liveFeeds = arrayListOf<Article>()
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = hashMapOf())).thenReturn(Observable.just(FeedResponse(articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onViewPrepared()

        // testing manageLoadingIndicator()
        verify(view, times(1)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(0)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(1)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(1)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onViewPrepared_noInternetHasCachedFeeds_displayNewsFirstPageCachedFeeds() {
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(false))
        val liveFeeds = arrayListOf<Article>()
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = hashMapOf())).thenReturn(Observable.just(FeedResponse(articles = liveFeeds)))
        val cachedFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onViewPrepared()

        // testing manageLoadingIndicator()
        verify(view, times(1)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(0)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(1)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(1)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onViewPrepared_hasInternetEmptyResponseNoCachedFeeds_noResultsDisplayed() {
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        val liveFeeds = arrayListOf<Article>()
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onViewPrepared()

        // testing manageLoadingIndicator()
        verify(view, times(1)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(0)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(1)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(1)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onViewPrepared_hasInternetUnexpectedErrorNoCachedFeeds_noResponseDisplayed() {
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val liveFeeds = arrayListOf<Article>()
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = hashMapOf())).thenReturn(Observable.just(FeedResponse(articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onViewPrepared()

        // testing manageLoadingIndicator()
        verify(view, times(1)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(0)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(1)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(1)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onViewPrepared_hasInternetGoodResponseNoCachedFeeds_displayNewsFirstPageLiveFeeds() {
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onViewPrepared()

        // testing manageLoadingIndicator()
        verify(view, times(1)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(1)).deleteArticlesFromRealm()
        verify(interactor, times(1)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(1)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(1)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onRefreshFeed_emptyKeywordGoodResponse_displayNewsFirstPageLiveFeeds() {
        val keyword = ""
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onRefreshFeed(keyword = keyword)

        // testing manageLoadingIndicator()
        verify(view, times(0)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(1)).deleteArticlesFromRealm()
        verify(interactor, times(1)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(1)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(0)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(1)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onRefreshFeed_goodKeywordGoodResponse_displayNewsFirstPageLiveFeeds() {
        val keyword = "query"
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["q"] = keyword
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onRefreshFeed(keyword = keyword)

        // testing manageLoadingIndicator()
        verify(view, times(0)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(1)).deleteArticlesFromRealm()
        verify(interactor, times(1)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(1)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(0)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(1)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun afterTextChanged_emptyInput_doNotPerformSearch() {
        val input = ""
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.afterTextChanged(input = input)

        // testing afterTextChanged()
        verify(view, times(1)).idleSearchIcon()
        verify(view, times(1)).hideClearIcon()
        verify(view, times(0)).activateSearchIcon()
        verify(view, times(0)).showClearIcon()
        // testing manageLoadingIndicator()
        verify(view, times(0)).prepareLoading()
        verify(view, times(0)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(0)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(0)).dismissLoadingIndicator()
        verify(view, times(0)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun afterTextChanged_goodInputGoodResponse_performSearch() {
        val input = "query"
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = "1"
        filters["q"] = input
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.afterTextChanged(input = input)

        // testing afterTextChanged()
        verify(view, times(0)).idleSearchIcon()
        verify(view, times(0)).hideClearIcon()
        verify(view, times(1)).activateSearchIcon()
        verify(view, times(1)).showClearIcon()
        // testing manageLoadingIndicator()
        verify(view, times(0)).prepareLoading()
        verify(view, times(1)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(1)).deleteArticlesFromRealm()
        verify(interactor, times(1)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(0)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(1)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(0)).dismissLoadingIndicator()
        verify(view, times(1)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onRetrieveNewsFeed_emptyKeywordGoodResponse_displayNewsNextPageLiveFeeds() {
        val page = 2
        val keyword = ""
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = page.toString()
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onRetrieveNewsFeed(page = page, keyword = keyword)

        // testing manageLoadingIndicator()
        verify(view, times(0)).prepareLoading()
        verify(view, times(1)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(1)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(1)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(0)).dismissLoadingIndicator()
        verify(view, times(1)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onRetrieveNewsFeed_goodKeywordGoodResponse_displayNewsNextPageLiveFeeds() {
        val page = 2
        val keyword = "query"
        `when`(view.isNetworkConnected()).thenReturn(Observable.just(true))
        val filters: HashMap<String, String> = hashMapOf()
        filters["country"] = "gb"
        filters["category"] = "sports"
        filters["page"] = page.toString()
        filters["q"] = keyword
        val liveFeeds = arrayListOf(Article(title = "This is a title"))
        `when`(interactor.callRetrieveTopHeadlines(filterParameters = filters)).thenReturn(Observable.just(FeedResponse(totalResults = 1, articles = liveFeeds)))
        val cachedFeeds = arrayListOf<Article>()
        `when`(interactor.retrieveArticlesFromRealm()).thenReturn(cachedFeeds)

        presenter.onRetrieveNewsFeed(page = page, keyword = keyword)

        // testing manageLoadingIndicator()
        verify(view, times(0)).prepareLoading()
        verify(view, times(1)).showLoadMoreIndicator()
        // testing manageOnSuccess()
        verify(interactor, times(0)).deleteArticlesFromRealm()
        verify(interactor, times(1)).insertArticlesInRealm(articles = liveFeeds)
        verify(view, times(1)).displayNewsNextPage(articles = liveFeeds)
        // testing manageOnError()
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_CONNECTION)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESULTS)
        verify(view, times(0)).showErrorView(state = ErrorViewState.NO_RESPONSE)
        // distinct testing
        verify(view, times(0)).displayNewsFirstPage(articles = liveFeeds)
        verify(view, times(0)).displayNewsFirstPage(articles = cachedFeeds)
        verify(view, times(0)).dismissLoadingIndicator()
        verify(view, times(1)).hideLoadMoreIndicator()
        verify(view, times(0)).dismissSwipeToRefreshIndicator()
    }

    @Test
    fun onArticleSelected_openArticleFragment() {
        val article = Article(title = "This is a title")

        presenter.onArticleSelected(article = article)

        verify(view, times(1)).openArticleFragment(article = article)
    }

    //        getView()?.let {
//            manageLoadingIndicator(view = it, loadingMode = loadingMode)
//
//            compositeDisposable.add(it.isNetworkConnected()
//                    .switchMap { isConnected ->
//                        if (isConnected) {
//                            interactor!!.callRetrieveTopHeadlines(filterParameters = filters)
//                                    .map { feedResponse -> RxUtils.checkApiResponseData(feedResponse) }
//                        }
//                        else throw Exceptions.propagate(NoNetworkConnectionException())
//                    }
//                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
//                    .compose(schedulerProvider.ioToMainObservableScheduler())
//                    .subscribe (
//                            { articlesResponse ->
//                                manageOnSuccess(view = it, loadingMode = loadingMode, articlesResponse = articlesResponse, page = page)
//                            },
//                            { err ->
//                                manageOnError(view = it, loadingMode = loadingMode, throwable = err)
//                            }
//                    )
//            )
//        }

//    when (loadingMode) {
//        AppConstants.LoadingMode.FULL_LOADING -> view.prepareLoading()
//        AppConstants.LoadingMode.PAGINATION_LOADING -> view.showLoadMoreIndicator()
//        AppConstants.LoadingMode.KEYWORD_SEARCH -> view.showLoadMoreIndicator()
//        AppConstants.LoadingMode.SWIPE_TO_REFRESH -> { /* do nothing */ }
//    }

//    if (page == 1)
//        interactor?.deleteArticlesFromRealm()
//
//    interactor?.insertArticlesInRealm(articles = articlesResponse)
//    view.displayNewsNextPage(articles = articlesResponse)
//
//    when (loadingMode) {
//        AppConstants.LoadingMode.FULL_LOADING -> view.dismissLoadingIndicator()
//        AppConstants.LoadingMode.PAGINATION_LOADING -> view.hideLoadMoreIndicator()
//        AppConstants.LoadingMode.KEYWORD_SEARCH -> view.hideLoadMoreIndicator()
//        AppConstants.LoadingMode.SWIPE_TO_REFRESH -> { view.dismissSwipeToRefreshIndicator() }
//    }

//        when (loadingMode) {
//            AppConstants.LoadingMode.FULL_LOADING -> {
//                val mArticleResponse = interactor?.retrieveArticlesFromRealm()
//                if (mArticleResponse.isNullOrEmpty()) {
//                    RxUtils.manageExceptions(throwable, view)
//                    view.dismissLoadingIndicator()
//                }
//                else {
//                    view.displayNewsFirstPage(articles = mArticleResponse)
//                    view.dismissLoadingIndicator()
//                }
//            }
//            AppConstants.LoadingMode.PAGINATION_LOADING -> view.hideLoadMoreIndicator()
//            AppConstants.LoadingMode.KEYWORD_SEARCH -> {
//                RxUtils.manageExceptions(throwable, view)
//                view.hideLoadMoreIndicator()
//            }
//            AppConstants.LoadingMode.SWIPE_TO_REFRESH -> {
//                val mArticleResponse = interactor?.retrieveArticlesFromRealm()
//                if (mArticleResponse.isNullOrEmpty()) {
//                    RxUtils.manageExceptions(throwable, view)
//                    view.dismissSwipeToRefreshIndicator()
//                }
//                else {
//                    view.displayNewsFirstPage(articles = mArticleResponse)
//                    view.dismissSwipeToRefreshIndicator()
//                }
//            }
//        }
//
//        fun manageExceptions(throwable: Throwable, view: NewsMVPView) {
//            when (throwable) {
//                is NoNetworkConnectionException -> { view.showErrorView(ErrorViewState.NO_CONNECTION) }
//                is NoResultsException -> { view.showErrorView(ErrorViewState.NO_RESULTS) }
//                else -> { view.showErrorView(ErrorViewState.NO_RESPONSE) }
//            }
//        }
}